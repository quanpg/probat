<?php
namespace DH\test;

use DH\CurlClient\CurlClient;
use DH\CurlClient\Log\Logger;

require '../../vendor/autoload.php';

/**
 * Class CurlInterfaceTest
 * Test para CurlClient
 */
class CurlInterfaceTest extends \PHPUnit_Framework_TestCase {

	public function testImplements() {
		$logger     = new Logger(true);
		$curlClient = new CurlClient($logger);
		
		$this -> assertInstanceOf('DH\CurlClient\ClientInterface', $curlClient);
	}

	/**
	 * Test autenticacion
	 */
	public function testUseAuthentication($user = "user", $pass = "pass") {
		$logger     = new Logger();
		$curlClient = new CurlClient($logger);
		$curlClient -> call();
		$useAuthentication = $curlClient -> useAuthentication($user, $pass);
		
		$this -> assertTrue($useAuthentication);
	}

	/**
	 * Test anhadir parametros
	 */
	public function testAddParam($name = "name", $value = "value") {
		$logger = new Logger();
		$curlClient = new CurlClient($logger);
		$curlClient -> setBaseUrl('https://gl.dinahosting.com');
		$curlClient -> call();
		$addParam = $curlClient -> addParam($name, $value);
		
		$this -> assertTrue($addParam);
	}

	/**
	 * Test de la llamada
	 */
	public function testCall() {
		$logger     = new Logger();
		$curlClient = new CurlClient($logger);
		$curlClient -> setBaseUrl('https://gl.dinahosting.com');
		$call = $curlClient -> call();
		
		$this -> assertTrue($call);
	}

	/**
	 * Test del reset
	 */
	public function testReset() {
		$logger = new Logger();
		$curlClient = new CurlClient($logger);
		$curlClient -> setBaseUrl('https://gl.dinahosting.com');
		$reset = $curlClient -> reset();
		
		$this -> assertEmpty($reset);
	}

	/**
	 * Test Fallo autenticacion
	 */
	public function testUseAuthenticationFailure($user = "user", $pass = "pass") {
		$logger     = new Logger();
		$curlClient = new CurlClient($logger);
		$curlClient -> call();
		$useAuthentication = $curlClient -> useAuthentication($user, $pass);
		
		$this -> assertFalse($useAuthentication);
	}

	/**
	 * Test Fallo anhadir parametros
	 */
	public function testAddParamFailure($name = "name", $value = "value") {
		$logger     = new Logger();
		$curlClient = new CurlClient($logger);
		$curlClient -> setBaseUrl('https://gl.dinahosting.com');
		$curlClient -> call();
		$addParam = $curlClient -> addParam($name, $value);
		
		$this -> assertFalse($addParam);
	}

	/**
	 * Test Fallo de la llamada
	 */
	public function testCallFailure() {
		$logger     = new Logger();
		$curlClient = new CurlClient($logger);
		$curlClient -> setBaseUrl('https://gl.dinahosting.com');
		$call = $curlClient -> call();
		
		$this -> assertFalse($call);
	}

	/**
	 * Test Fallo del reset
	 */
	public function testResetFailure() {
		$logger     = new Logger();
		$curlClient = new CurlClient($logger);
		$curlClient -> setBaseUrl('https://gl.dinahosting.com');
		$reset = $curlClient -> reset();
		
		$this -> assertNotEmpty($reset);
	}

}
