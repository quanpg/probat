<?php
namespace DH\CurlClient;

trait ParamsAwareTrait {
  protected $params = array();

  public function setParams(array $params) {
    $this -> params = $params;
  }

  public function getParams() {
    return $this -> params;
  }

}
